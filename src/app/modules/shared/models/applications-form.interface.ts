export interface ApplicantsFormInterface {
  readonly customerId: number;
  readonly nameCorporation: string;
  readonly beginDate: string;
  readonly endDate: string;
}
