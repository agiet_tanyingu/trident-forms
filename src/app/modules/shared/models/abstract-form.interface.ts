export interface AbstractFormInterface {
  readonly code: string;
  readonly name: string;
  readonly reportModule: string;
}
