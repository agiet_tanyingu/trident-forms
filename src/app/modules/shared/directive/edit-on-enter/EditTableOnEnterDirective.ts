import { Directive, HostListener } from '@angular/core';
import { EditTableComponent } from '../../components/edit-table/edit-Table.component';


@Directive({
  selector: '[editTableOnEnter]'
})
export class EditTableOnEnterDirective {
  @HostListener('keyup.enter') public onEnter(): void {
    this.editTable.toViewMode();
  }
  constructor(private editTable: EditTableComponent) { }
}
