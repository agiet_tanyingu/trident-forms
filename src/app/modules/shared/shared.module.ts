import { AbstractFormService } from './services/abstract-from.service';
import { EditTableOnEnterDirective } from './directive/edit-on-enter/EditTableOnEnterDirective';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { EditModeDirective } from './directive/edit-mode/edit-mode.directive';
import { ViewModeDirective } from './directive/view-mode/view-mode.directive';
import { EditTableComponent } from './components/edit-table/edit-Table.component';
import { AbstractFormComponent } from './components/abstract-form/abstract-form.component';
import { ApplicationsFormComponent } from './components/applications-form/applications-form.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { ApplicantsFormServiceAbsrtact } from './services/abstract/applicants-form.service.abstract';
import { ApplicantsFormService } from './services/applicants-form.service';
import { AbstractFormServiceAbstract } from './services/abstract/abstract-from.service.abstract';
import { FormFieldComponent } from './components/form-field/form-field.component';
@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
  ],
  declarations: [
    ViewModeDirective,
    EditModeDirective,
    EditTableComponent,
    EditTableOnEnterDirective,
    ApplicationsFormComponent,
    AbstractFormComponent,
    FormFieldComponent
  ],
  exports: [
    MatTableModule,
    ViewModeDirective,
    EditModeDirective,
    EditTableComponent,
    EditTableOnEnterDirective,
    ApplicationsFormComponent,
    AbstractFormComponent,
    FormFieldComponent
  ],
  providers: [
    { provide: ApplicantsFormServiceAbsrtact, useClass: ApplicantsFormService },
    { provide: AbstractFormServiceAbstract, useClass: AbstractFormService}
  ]
})
export class SharedModule {}
