
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApplicantsFormInterface } from '../models/applications-form.interface';
import { ApplicantsFormServiceAbsrtact as ApplicantsFormServiceAbsrtact } from './abstract/applicants-form.service.abstract';

@Injectable()
export class ApplicantsFormService implements ApplicantsFormServiceAbsrtact {
  public applicants: ApplicantsFormInterface[] = [];
  constructor() {
    this.applicants =  [
      {
        customerId: 1234567,
        nameCorporation: 'james d ferch',
        beginDate: '3/27/2009',
        endDate: '11/29/2012',
      },
      {
        customerId: 1234567,
        nameCorporation: 'tommi t haikka',
        beginDate: '11/29/2012',
        endDate: 'n/a',
      },
      {
        customerId: 1234567,
        nameCorporation: 'maggie j haikka',
        beginDate: '11/29/2012',
        endDate: 'n/a',
      },
    ];
  }

  // convert to http get
  public get applicants$(): BehaviorSubject<ApplicantsFormInterface[]> {
    const data: BehaviorSubject<ApplicantsFormInterface[]> = new BehaviorSubject(this.applicants);
    return data;
  }

  public update(index: number, field: string, value: string): void {
    this.applicants = this.applicants.map((e: ApplicantsFormInterface, i: number) => {
      if (index === i) {
        return {
          ...e,
          [field]: value
        }
      }
      return e;
    });
    this.applicants$.next(this.applicants);
  }
}
