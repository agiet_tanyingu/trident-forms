import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ApplicantsFormInterface } from 'src/app/modules/shared/models/applications-form.interface';

export abstract class ApplicantsFormServiceAbsrtact {
  public abstract get applicants$(): BehaviorSubject<ApplicantsFormInterface[]>;
  public abstract update(index: number, field: string, value: string): void;
}
