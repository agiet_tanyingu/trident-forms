import { Observable } from 'rxjs/internal/Observable';
import { AbstractFormInterface } from '../../models/abstract-form.interface';

export abstract class AbstractFormServiceAbstract {
  public abstract get data$(): Observable<AbstractFormInterface[]>
}
