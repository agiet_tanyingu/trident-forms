import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { AbstractFormInterface } from '../models/abstract-form.interface';
import { AbstractFormServiceAbstract } from './abstract/abstract-from.service.abstract';

@Injectable()
export class AbstractFormService implements AbstractFormServiceAbstract{
  constructor() {}
  public data: AbstractFormInterface[] = [
      {
        code: 'code 1',
        name: 'name 1',
        reportModule: 'report module 1',
      },
      {
        code: 'code 2',
        name: 'name 2',
        reportModule: 'report module 2',
      },
      {
        code: 'code 3',
        name: 'name 3',
        reportModule: 'report module 3',
      },
    ];

  public get data$(): Observable<AbstractFormInterface[]> {
    const data: BehaviorSubject<AbstractFormInterface[]> = new BehaviorSubject(this.data);
    return data.asObservable();
  }

}
