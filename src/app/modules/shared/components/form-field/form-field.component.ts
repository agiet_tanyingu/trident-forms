import { FormFieldAppearanceEnum } from './../../enums/form-field-appearance.enum';
import { FormFieldTypeEnum } from '../../enums/form-field-type.enum';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
})
export class FormFieldComponent implements OnInit {
  @Input() public data = '';
  @Input() public formName?: string;
  @Input() public label?: string;
  @Input() public appearanceType?:
    | FormFieldAppearanceEnum.Fill
    | FormFieldAppearanceEnum.Outline
    | FormFieldAppearanceEnum.None;
  @Input() public isDisabled = false;
  @Input() public type?:
    | FormFieldTypeEnum.Input
    | FormFieldTypeEnum.TextArea
    | FormFieldTypeEnum.Select = FormFieldTypeEnum.Input;
  @Input() public hint?: string;
  @Input() public icon?: string;
  @Input() public isRequired = false;

  public constructor() {}

  public ngOnInit(): void {}

  public get isInputType(): boolean {
    return this.type === FormFieldTypeEnum.Input;
  }

  public get isSelectType(): boolean {
    return this.type === FormFieldTypeEnum.Select;
  }

  public get isTextAreaType(): boolean {
    return this.type === FormFieldTypeEnum.TextArea;
  }
}
