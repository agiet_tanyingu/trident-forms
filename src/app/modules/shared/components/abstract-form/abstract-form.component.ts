import { AbstractFormInterface } from './../../models/abstract-form.interface';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl } from '@angular/forms';
import { AbstractFormServiceAbstract } from '../../services/abstract/abstract-from.service.abstract';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-abstract-form',
  templateUrl: './abstract-form.component.html',
  styleUrls: ['./abstract-form.component.scss'],
})
export class AbstractFormComponent implements OnInit {
  public form: FormGroup = new FormGroup({
    abstractForms: new FormArray([])
  });
  public abstractForms: FormArray = this.form.controls
    .abstractForms as FormArray;

  constructor(private abstractFormService: AbstractFormServiceAbstract, private snackBar: MatSnackBar) {}
  public ngOnInit(): void {
    this.abstractFormService.data$.subscribe((data: AbstractFormInterface[]) =>
      this.generateForm(data)
    );
  }

  public generateForm(data: AbstractFormInterface[]): void {
    data.forEach((abstractForm: AbstractFormInterface) => {
      this.abstractForms.push(
        new FormGroup({
          code: new FormControl(abstractForm.code),
          name: new FormControl(abstractForm.name),
          reportModule: new FormControl(abstractForm.reportModule),
        })
      );
    });
  }

  public addForm(): void {
    this.abstractForms.push(
      new FormGroup({
        code: new FormControl(''),
        name: new FormControl(''),
        reportModule: new FormControl(''),
      })
    );
  }

  public removeForm(index: number): void {
    this.abstractForms.removeAt(index);
  }

  public onSubmit(): void {

    this.snackBar.open(
      'Abstract Report Form Submitted',
      null,
      {
        duration: 5000,
        horizontalPosition: 'end',
        verticalPosition: 'top'
      });
  }
}
