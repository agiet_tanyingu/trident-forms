
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Component, OnInit } from '@angular/core';
import { ApplicantsFormServiceAbsrtact } from 'src/app/modules/shared/services/abstract/applicants-form.service.abstract';
import { ApplicantsFormInterface } from '../../models/applications-form.interface';

@Component({
  selector: 'app-applications-form',
  templateUrl: './applications-form.component.html',
  styleUrls: ['./applications-form.component.scss'],
})
export class ApplicationsFormComponent implements OnInit {
  public columnNames: string[] = [
    'Customer ID',
    'Name / Corporation',
    'Begin Date',
    'End Date',
  ];
  public dataSource$: BehaviorSubject<ApplicantsFormInterface[]> = this
    .applicantsService.applicants$;
  public controls: FormArray;
  constructor(private applicantsService: ApplicantsFormServiceAbsrtact) {}

  public ngOnInit(): void {
    const forms: FormGroup[] = this.applicantsService.applicants$.value.map((applicant: ApplicantsFormInterface) => {
      return new FormGroup({
        customerId: new FormControl(applicant.customerId, Validators.required),
        nameCorporation: new FormControl(applicant.nameCorporation, Validators.required),
        beginDate: new FormControl(applicant.beginDate, Validators.required),
        endDate: new FormControl(applicant.endDate, Validators.required)
      }, { updateOn: 'blur'});
    });
    this.controls = new FormArray(forms);
  }

  public updateField(index: number, field: string): void {
    const control: FormControl = this.getControl(index, field);
    if (control.valid) {
      this.applicantsService.update(index, field, control.value)
    }
  }

  public getControl(index: number, fieldName: string): FormControl {
    const a: FormControl = this.controls.at(index).get(fieldName) as FormControl;
    return this.controls.at(index).get(fieldName) as FormControl;
  }
}
