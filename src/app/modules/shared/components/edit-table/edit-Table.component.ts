import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { fromEvent, Observable, Subject } from 'rxjs';
import { filter, switchMapTo, take } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ViewModeDirective } from '../../directive/view-mode/view-mode.directive';
import { EditModeDirective } from '../../directive/edit-mode/edit-mode.directive';

@Component({
  selector: 'app-edit-table',
  templateUrl: './edit-Table.component.html',
  styleUrls: ['./edit-Table.component.scss'],
})
export class EditTableComponent implements OnInit, OnDestroy {
  @ContentChild(ViewModeDirective) public viewModeDirective: ViewModeDirective;
  @ContentChild(EditModeDirective) public editModeDirective: EditModeDirective;
  @Output() public update = new EventEmitter();
  public editMode: Subject<boolean> = new Subject<boolean>();
  public editMode$: Observable<boolean> = this.editMode.asObservable();

  // mode can only be 'view' or 'edit'
  public mode: 'view' | 'edit' = 'view';

  constructor(private host: ElementRef) {}

  public ngOnInit(): void {
    this.viewModeHandler();
    this.editModeHandler();
  }

  public get currentView(): TemplateRef<any> {
    return this.mode === 'view'
      ? this.viewModeDirective.template
      : this.editModeDirective.template;
  }

  public toViewMode(): void {
    this.update.next();
    this.mode = 'view';
  }

  private get element(): any {
    return this.host.nativeElement;
  }

  private viewModeHandler(): void {
    // listener should be button 'edit button'
    fromEvent(this.element, 'dblclick')
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.mode = 'edit';
        this.editMode.next(true);
      });
  }

  private editModeHandler(): void {
    const clickOutside$ = fromEvent(document, 'click').pipe(
      filter(({ target }) => this.element.contains(target) === false),
      take(1)
    );
    this.editMode$
      .pipe(switchMapTo(clickOutside$), untilDestroyed(this))
      .subscribe((event) => this.toViewMode);
  }

  public ngOnDestroy(): void {}
}
