import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import { NavigationServiceAbstract } from './services/abstract/navigation.service.abstract';
import { NavigationService } from './services/navigation.service';
import { RouterModule } from '@angular/router';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  imports: [CommonModule, RouterModule, MatIconModule, MatMenuModule, MatRadioModule],
  declarations: [PageNotFoundComponent, HeaderComponent, NavComponent],
  providers: [
    { provide: NavigationServiceAbstract, useClass: NavigationService },
  ],
  exports: [PageNotFoundComponent, HeaderComponent, NavComponent],
})
export class CoreModule {}
