export interface NavInterface {
  name: string;
  route: string;
  isHovered?: boolean;
}
