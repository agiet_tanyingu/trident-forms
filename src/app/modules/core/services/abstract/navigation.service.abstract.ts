export abstract class NavigationServiceAbstract {
  public abstract navigateToHome(): void;
  public abstract navigateToApplications(): void;
}
