import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class NavigationService {
  constructor(private router: Router) {}

  public navigateToHome(): void {
    this.router.navigate(['/home']);
  }

  public navigateToApplications(): void {
    this.router.navigate(['/applications']);
  }
}
