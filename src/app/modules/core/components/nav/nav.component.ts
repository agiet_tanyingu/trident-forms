import { Component, OnInit, Input } from '@angular/core';
import { NavInterface } from '../../models/nav.interface';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  @Input() public styleOnHover: string;
  public routes: NavInterface[];
  public hover = false;
  constructor() {}s
  public ngOnInit(): void {
    this.routes = [
      { name: 'home', route: 'home' },
      { name: 'applications', route: 'applications' },
      { name: 'water rights', route: 'water-rights' },
      { name: 'owners / contacts', route: 'owners-contacts' },
      { name: 'reports', route: 'reports' },
      { name: 'more ...', route: 'more'}
    ];
  }

  public setHover(route: NavInterface, status: boolean): void {
    route.isHovered = status;
  }

}
