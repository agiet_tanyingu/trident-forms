import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Output()  public updateStyle: EventEmitter<string> = new EventEmitter();
  public bgColor = '#AEBA95';
  public fontColor = '#0D2063';

  constructor() {}

  public setBgColor(color: string): void {
    this.bgColor = color;
    this.updateStyle.emit(this.bgColor)
  }
  public setFontColor(): string {
    return this.fontColor;
  }
}
