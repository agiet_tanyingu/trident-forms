import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeConatinerComponent } from './components/home-container.component';
import { HomeRoutingModule } from './components/home-routing.module';

@NgModule({
  imports: [CommonModule, HomeRoutingModule],
  declarations: [HomeConatinerComponent],
  bootstrap: [HomeConatinerComponent]
})
export class HomeModule {}
