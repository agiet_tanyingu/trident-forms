import { CoreModule } from './../../core/core.module';

import { ApplicationRoutingModule } from './applications-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ApplicationSearchComponent } from './components/search/application-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ApplicantsFormServiceAbsrtact } from '../../shared/services/abstract/applicants-form.service.abstract';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ApplicationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [ApplicationSearchComponent],
  providers: [
   ,
  ],
})
export class ApplicationModule {}
