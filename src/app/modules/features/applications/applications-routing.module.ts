import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';
import { ApplicationSearchComponent } from './components/search/application-search.component';

const routes: Routes = [
  {
    path: '',
    component: ApplicationSearchComponent,
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
