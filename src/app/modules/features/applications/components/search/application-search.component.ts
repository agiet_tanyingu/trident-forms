import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './application-search.component.html',
  styleUrls: ['./application-search.component.scss'],
})
export class ApplicationSearchComponent implements OnInit {
  public activitiesForm = new FormGroup({
    activities: new FormArray([]),
  });

  public activities = this.activitiesForm.get('activities') as FormArray;
  public unsubscribe$: Subject<any> = new Subject();

  constructor() {}

  public ngOnInit(): void {
  }

  public get form(): AbstractControl[] {
    return this.activities.controls;
  }
}
