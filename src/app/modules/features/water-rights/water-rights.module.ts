import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { WaterRightsContainerComponent } from './components/water-rights-container.component';

@NgModule({
  imports: [CommonModule],
  declarations: [WaterRightsContainerComponent],
})
export class WaterRightsModule {}

