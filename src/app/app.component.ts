import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public hoverColor = '#AEBA95';
  public title = 'trident-forms';
  public constructor() {

  }
  public updateStyle(color: string): void {
    console.log(color)
    this.hoverColor = color;
  }

}
