import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PageNotFoundComponent } from './modules/core/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'applications',
    children: [
      {path: '', loadChildren: () => import('./modules/features/applications/applications.module').then(
            (m) => m.ApplicationModule
          ),
      },
    ],
  },
  {
    path: 'home',
    children: [
      {
        path: '', loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
      },
    ],
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
